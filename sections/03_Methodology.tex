\section{Methodology}
\label{sec:methodology}

We propose a dynamic demand simulation with three major components. They are
visualized in Figure \ref{fig:amod_loop}. The transport system, including an
AMoD service, is simulated in the \textit{mobility simulation}. As will be
described in detail below, we use a mesoscopic simulator to be able to track
the decisions and movements of a large number of travellers, which are
represented as agents. They interact with each other and the vehicle agents
of an automated vehicle service, which is simulated in detail. From the full
day simulations, we can measure a number of metrics such as the distance driven
with a customer or the empty distance of the vehicle fleet. Those metrics are
fed into the \textit{cost calculator} component, which, based on data for
Switzerland, is able to estimate the ninimum price that an operator would need to ask
the customers to sustain a cost-covering service. This price, along with other
choice attributes that can be measured from simulation, is fed into a \textit{discrete
choice model} in which individual travellers with their individual mobility
patterns for each trip which mode of transport to use.

By running this loop of models iteratively, we can analyze the complex interplay
between supply and demand in an AMoD system. Initially a price would be defined
for the service, which would motivate a certain number of travellers to use the
service. However, the more travellers use the service, the higher the waiting
times become. Thus, attractiveness decreases and the likelihood of choosing this
mode of transport, too. Eventually, the system stabilizes in a dynamic equilibirum
in which prices, waiting times, and demand are in a consistent state. These values
are then analyzed, for instance for various fleet sizes which are kept fixed in
their individual simulation runs. Additionally, secondary metrics can be measured
such as the ratio of empty distance, changes in mode shares the total distance
driven in the system, and others. This way, it is possible to quantify the
systemic impact of an AMoD system on a city, in this case Zurich.

The three components, (a) \textit{cost calculator}, (b) \textit{discrete choice
model}, and (c) \textit{mobility simulation} will be described in detail in the
following sections.

\begin{figure}
  \centering
  \includegraphics[width=4.5in]{figures/AstraModelSetup.pdf}
  \caption{The three components of the model are shown: cost calculator, discrete choice model and agent-based transport simulation. The arrows indicate the flow of information between the three components which are executed iteratively.}
  \label{fig:amod_loop}
\end{figure}

\subsection{Cost structure model}

The cost model used in this study was developed specifically for the context
of Switzerland. It is a bottom-up study of the cost components that lead to
the final cost of various forms of mobility, from private vehicle ownership,
to taxis, trains and busses. The full costs per passenger kilometers are thereby
derived from investment costs, cleaning costs, maintenance costs, and others.

While the cost study considers urban and non-urban environments, only the results for
the former are of interest in this study. The major results are the following.
\hl{B\"osch et al.)} find that public transport may see a cost reduction from today around
0.53 CHF/pkm to 0.24 CHF/pkm with automated vehicle technology being widely
adopted in the system. On the contrary, costs for private vehilce ownership
would stay about the same in Switzerland as today, at around 0.5 CHF/km. The
largest decrease though would happen for taxi services. \hl{B\"osch et al.} predict that
the rather costly costs of taxi services in Switzerland would drop from around
2.73 CHF/pkm to only 0.41 CHF/pkm, which would be highly competitive
in comparison to the private car. Hence, it is important to study such a service
as operators would be willing to offer such a service from an economic
perspective.

It should be noted that the cost in \hl{B\"osch et al.} are based on best-guess
predictions of fleet utilization and empty distance, which highly effect the
cost structure of the service. Therefore, we use the cost model from \hl{B\"osch et al.}
in this study to feed the model with exact data from the agent-based transport
simulation to arrive at prices that are in line with the actual simulated
usage of the fleet. The cost structure presented in \hl{B\"osch et al.} can
be broken down to three cost components: costs per kilometer per day, costs
per trip per day (mainly cleaning), and costs per vehicle per day. The total
fleet cost per day can therefore be described by

\begin{equation}
C_\text{Fleet} = c_\text{perDistance} \cdot x_\text{fleetDistance}
+ c_\text{perTrip} \cdot x_\text{numberOfThreads}
+ c_\text{perVehicle} \cdot x_\text{fleetSize}
\end{equation}

From \hl{B\"osch et. al} we derive the following parameter values:

\begin{itemize}
  \item $c_\text{perDistance} = 0.098\ \text{CHF/vkm}$
  \item $c_\text{perTrip} = 0.375\ \text{CHF}$
  \item $c_\text{perVehicle} = 33.30\ \text{CHF}$
\end{itemize}

In our simulations we consider a cost-covering automated taxi service, i.e.
we calculate a price per passenger kilometer as

\begin{equation}
p_\text{AMoD} = \frac{C_\text{Fleet}}{x_\text{customerDistance}} \ \ \ \text{in \ \ \ [CHF/pkm]}
\end{equation}

Hence, the price of the service depends on the fleet characteristics of
total driven vehicle distance, customer distance, number of trips and fleet size.

\subsection{Discrete choice model}

\hl{To check and refine for Felix}

To gain insights on how people would use an AMOD service in Zurich, a survey
was performed in the canton of Zurich with \hl{N} respondents. This survey
was conducted in two phases: During the first phase, respondents provided
sociodemographic information and a regular trip below and above 50km, respectively,
along with the mode of transport the usually would choose. In a second phase they
took part in a choice experiment in which alternatives were presented for their
regular trip. Those alternatives included private automated cars, automated
taxi and pooling services, either in combination with public transport or
without. For all alternatives, including the AMoD (single automated taxi),
different levels of prices and waiting times were presented to the respondents
to understand their preferences regarding such a service. While a more detailed
analysis of the survey results is in preparation, including extended models,
a multinomial logit model that is compatible with the chosen simulation architecture
was estimated for the simulations in this study.

First, the data was filtered such that only observations were used in which
either the currently chosen alternative was selected, or the AMoD option. This
assumes independence of alternatives such that the presence of the other options
does not affect the choice for or against AMoD in these situations. \hl{Do we
have to write more about that?}.

Second, the data was weighted according to sociodemographic attributes of the
Zurich region. The reference values were derived from the national household
travel survey, with trips selected that start and end inside of the study
area. \hl{Or did we weight by trip starting/ending in the canton? This
would be what we have in the survey, wouldn't it?} For reweighting, we chose
Iterative Proportional Fitting with the attributes \hl{I have the weighting
data in Zurich, will check next week}.

The model is defined by utilities for the modes \textit{car}, \textit{public transport},
\textit{bicycle}, \textit{walking}, and \text{AMoD}.

The utility for \textit{car} is defined by the equation

\begin{equation}\begin{aligned}
    u_{car} =& \beta_\text{ASC,car} \\
    &+ \beta_\text{inVehicleTime,car} \cdot \xi_\text{TD} \cdot x_\text{inVehicleTime,car} \\
    &+ \beta_\text{work,car} \cdot x_\text{work} + \beta_\text{city,car} \cdot x_\text{city} \\
    &+ \beta_\text{cost} \cdot \xi_\text{CD} \cdot \xi_\text{CI} \cdot x_\text{cost,car} \\
\end{aligned}\end{equation}

with $\beta$ describing one part of the model parameters to estimate, and $x$ the
trip-level attributes. The $\xi$ describe elasticities as defined further below.
The attribute $x_\text{work}$ defines whether the trip originates or ends at
a \textit{work} activity, and the attribute $x_\text{city}$ describes whether
the trip starts or ends inside of the city area of Zurich \hl{Ref to map}.

For public transport, the following equation has been defined:

\begin{equation}\begin{aligned}
    u_{pt} =& \beta_\text{ASC,pt} \\
    &+ \beta_\text{inVehicleTime,train} \cdot \xi_\text{TD} \cdot x_\text{inVehicleTime,train} \\
    &+ \beta_\text{inVehicleTime,other} \cdot \xi_\text{TD} \cdot x_\text{inVehicleTime,other} \\
    &+ \beta_\text{inVehicleTime,feeder} \cdot x_\text{inVehicleTime,feeder} \\
    &+ \beta_\text{waitingTime,pt} \cdot x_\text{waitingTime,pt} \\
    &+ \beta_\text{accessEgressTime,pt} \cdot x_\text{accessEgressTime,pt} \\
    &+ \beta_\text{headway,pt} \cdot x_\text{headway,pt} \\
    &+ \sum_G \beta_\text{ptQuality,G} \cdot x_\text{ptQuality,G} \\
    &+ \beta_\text{cost} \cdot \xi_\text{CD} \cdot \xi_\text{CI} \cdot x_\text{cost,pt} \\
\end{aligned}\end{equation}

The travel time for public transport is defined such that different public modes
of transport are taken into account. The attribute $x_\text{inVehicleTime,train}$
determines how much time the traveler spends in a train. If the connection \textit{additionally}
contains legs of other modes, such as busses or trams, the travel time in these
vehicles is considered as $x_\text{inVehicleTime,feeder}$, while $x_\text{inVehicleTime,other}$
is zero. \textit{Only} if there is no train leg on the chosen route, travel time
in busses, trams or ferries is considered as $x_\text{inVehicleTime,other}$ while
$x_\text{inVehicleTime,feeder}$ is set to zero.

The attribute $x_\text{ptQuality}$ relates to a methodology defined by the
Federal Office of Land Use in Switzerland that quantifies the accessibility to
public transport at any place in Switzerland, based on proximity to public
transport stops and stations and the frequency of the respective lines. It is
defined on five levels $G \in \{ \text{A}, \text{B}, \text{C}, \text{D}, \text{None} \}$
with $\text{A}$ the highest.

The utilities for cycling and walking are defined as

\begin{equation}\begin{aligned}
  u_{bicycle} =& \beta_\text{ASC,bicycle} \\
  &+ \beta_\text{travelTime,bicycle} \cdot \xi_{TD} \cdot x_\text{travelTime,bicycle} \\
  &+ \beta_\text{highAge,bicycle} \cdot [ a_\text{age} \geq 60 ] \\
\end{aligned}\end{equation}

and

\begin{equation}\begin{aligned}
  u_{walk} =& \beta_{ASC,walk} \\
  &+ \beta_{travelTime,walk} \cdot \xi_{TD} \cdot x_\text{travelTime,walk} \\
\end{aligned}\end{equation}

The variables $a$ refer to agent-level attributes, and for the case of
the \textit{bicycle} mode to the age of each agent, specifically.

The elasticities of Euclidean distance on travel time $\xi_{TD}$ and on
cost $\xi_{CD}$ are defined as

\begin{equation}
\xi_{TD} = \left( \frac{x_\text{euclideanDistance}}{\theta_\text{referenceDistance}} \right)^{\lambda_\text{TD}}
\hspace{1cm} \text{and} \hspace{1cm}
\xi_{CD} = \left( \frac{x_\text{euclideanDistance}}{\theta_\text{referenceDistance}} \right)^{\lambda_\text{CD}}
\end{equation}

with $\lambda$ describing additional model parameters that need to be
estimated. The reference values have been fixed prior to model estimation.

The elasticity of household income on cost $\xi_{CI}$ is defined as

\begin{equation}
\xi_{CI} = \left( \frac{a_\text{householdIncome}}{\theta_\text{referenceIncome}} \right)^{\lambda_\text{CI}}
\end{equation}

While the model presented so far refers to the modes that are available in the
baseline case, an additional \textit{AMoD} utility can be defined based on the
survey:

\begin{equation}\begin{aligned}
  u_{AMoD} =& \beta_\text{ASC,AMoD} \\
  &+ \beta_\text{inVehicleTime,AMoD} \cdot \xi_\text{TD} \cdot x_\text{inVehicleTime,AMoD} \\
  &+ \beta_\text{accessEgressTime,AMoD} \cdot x_\text{accessEgressTime,AMoD} \\
  &+ \beta_\text{waitingTime,AMoD} \cdot x_\text{waitingTime,AMoD} \\
  &+ \beta_\text{work,AMoD} \cdot x_\text{work} \\
  &+ \beta_\text{highAge,AMoD} \cdot [ a_\text{age} \geq 60 ] \\
  &+ \beta_\text{cost} \cdot \xi_\text{CD} \cdot \xi_\text{CI} \cdot x_\text{cost,AMoD} \\
\end{aligned}\end{equation}

The estimated model parameters are documented in \hl{TODO TABLE}. For the scope
of this research, it is interesting to compare the value of travel time
savings (VTTS) defined as

\begin{equation}
  VTTS = \frac{\partial u_m}{\partial \beta_\text{travelTime,m}} /
  \frac{\partial u_m}{\partial \beta_\text{cost,m}}
\end{equation}

of various modes to each other. For an average income, the
AMoD service is valued as more comfortable than the private car at any
distance (Figure \ref{fig:vtts}). In fact, the VTTS is comparable to a bus, but considerably higher trains,
which are used frequently and offer a high level of service in Zurich. This indeed
shows that there is a market for an AMoD service, also from the behavioural
perspective: people will want to use such a service and would potentially prefer
it to using their private car. However, Figure \ref{fig:vtts} also shows that
the VWTS (value of waiting time savings) defined as

\begin{equation}
  VWTS = \frac{\partial u_m}{\partial \beta_\text{waitingTime,m}} /
  \frac{\partial u_m}{\partial \beta_\text{cost,m}}
\end{equation}

is substantially higher for the AMoD
service than for public transport. This finding indicates unsurprisingly that
a major objective of any AMOD operator should be to minimize customer waiting
time as they are seen highly unattractive by travellers.

\begin{figure}
  \includegraphics[width=\textwidth]{figures/vtts.png}
  \caption{\hl{TODO. Properly plot this with correct colors etc. Just a placeholder}}
  \label{fig:vtts}
\end{figure}

\subsection{Agent-based simulation model}

For the simulation, the agent- and activity-based transport simulation framework
MATSim is used.
MATSim is an agent-based transport simulation framework, in which every traveler in the real world is represented by an artificial agent. Those agents have sociodemographic attributes such as age and gender, and they have a daily mobility plan. These daily mobility plans consist of activities, which hold information about the place of those activities, their start times, durations, and types. Furthermore, those activities are connected by trips which are described by a specific mode of transport and a route through the transport system.

The simulation considers 24 hours of the day in a time-step based manner. The day is simulated second by second, in which agents either are in an activity (residing at one specific location) or in transport (for instance, in the road network or a public transport vehicle). The movements of vehicles (like private cars, busses, or trains) are simulated along a directed graph network representing the roads and tracks of the infrastructure. Such infrastructure has capacities (for instance, limited space on roads) such that interactions between agents lead to congestion or crowding.
Congestion is simulated through a queue-based traffic simulation.

%While the standard approach of MATSim follows


%This work is furthermore based on a number of extension to MATSim. First, the
%simulations make use of an extension that makes it possible to simulate dynamic
%agents, which do not follow a statically planned mobility schedule for every day,
%but can be controlled step by step. Here, this functionality is used to simulate
%automated vehicles, which are controlled by a central dispatcher that receives
%requests when traveller agents want to depart at a specific location in the
%network and sends commands to vehicles to pick up customers and drop them off
%later on.


\begin{itemize}
  \item Use MATSim with tools developed in (...) and shown for static demand and free speeds in (...)
  \item Short description of data sets and their use (see thesis) for more details see (Paris paper)
  \item Now choice model added (see above), first in baseline case to calibrate the model
  \item Show calibration results and values (cost barrier for wlak/bike!)
  \item Important: This is a mode choice experiment! No departure time etc
  \item Used now with AV simulation components to simlate fleet
  \item Uses heuristic for fleet control would be interesting to do more detailed study in the future
\end{itemize}

\subsection{Model integration}

\begin{itemize}
  \item Show examples of convergence and stabilization of prices, mode shares and waiting times
  \item Describe how they are measured (division of the area in zones)
  \item Mention averaging of the input values
  \item Choice model constraints: AV Service area, minimum distance
\end{itemize}
